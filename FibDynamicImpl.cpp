//
// Created by James Daehn on 9/26/16.
//

#include "FibDynamicImpl.h"

unsigned long FibDynamicImpl::getFibonacciNumber(const unsigned long &n) const {
    unsigned long f[n];
    f[0] = 1;
    f[1] = 1;
    for (unsigned long i = 2; i < n; ++i) {
        f[i] = f[i-1] + f[i-2];
    }
    return f[n-1];
}

std::vector<unsigned long> FibDynamicImpl::getFibonacciSequence(const unsigned long &n) const {
    return std::vector<unsigned long>();
}