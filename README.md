# Lab 5: Recursion and Measuring Algorithm Efficiency

## Background

The following is adapted from *Lab Manual ADTs, Data Structures and Problem Solving with C++*, second edition by Larry Nyhoff, Pearson Prentice Hall, (c) 2006, pg. 139.

Efficiency is an important property of algorithms and, by extension, an important property of programs. Efficiency takes many forms including

   * space efficiency
   * time efficiency
   * resource efficiency, such as use of disk drives and processors
  
Time efficiency --- that is, speed of execution --- is a common one. One function is more efficient than another in solving a given problem if it solves the problem in less time than the other function. In this lab we will actually tme some algorithms to gain information about their efficiency.

There is no coding in this lab. Instead, you'll run a series of "experiments" to collect data and analyze your observations. To get started, you can just compile and run the lab with the following command:

```bash
$ g++ -std=c++11 *.cpp -o lab; ./lab
```

at which point, you should see 

```
Welcome to Lab 5!


Select from the menu:

1. Find fib(n) using recursive implementation
2. Find fib(n) using iterative implementation
3. Find fib(n) using dynamic implementation
4. Print table in range given by command line arguments
5. Play Towers of Hanoi
6. See how many moves it takes to solve Towers of Hanoi puzzle.
7. Find execution times for Towers of Hanoi game
8. Quit

		What do you want to do?  
```

As you can gather from the menu options, we have three implementations for finding the nth Fibonacci number, a means for printing a table of some sort, and two options to work with the Towers of Hanoi problem. Take a moment to study each of these implementations:

 1. `IFib.h` - This is an interface defining the basic ADT API for our Fibonacci utility classes
 1. `FibRecursiveImpl.h`/`FibRecursiveImpl.cpp` - This is an implementation of the IFib interface that employs recursion.
 1. `FibIterativeImpl.h`/`FibIterativeImpl.cpp` - This is an implementation of the IFib interface that employs an iterative solution.
 1. `FibDynamicImpl.h`/`FibDynamicImpl.cpp` - This is another implementation of the IFib interface that also uses iteration using techniques from dynamic programming.
 1. `TowersOfHanoi.h`/`TowersOfHanoi.cpp` - This class allows one to simulate playing the Towers of Hanoi game.
 
After studying these sources, interact a bit with the program. You'll be asked to gather a variety of information, so knowing what you can get out of the program first may make your time more productive in the end.
 
## Getting Started

This lab, like all previous labs, requires that you fork this repo into a private repo in your own account, clone that forked repo onto your local development machine and create a development branch within in which to work. At various points in time you'll be asked to modify the `Questions.txt` file, save your changes and commit them using the usual `git` commands. If you don't recall this modus operandi, see any of your previous labs. Ultimately you'll only be modifying the `Questions.txt` file. When you have completed the lab, as usual, you will create a pull request to merge *your* develop branch into *your* master branch. The lab is consider complete when:

 1. You have created your pull request.
 1. You have submitted your assignment on Blackboard. As usual, this requires that (a) provide me with the URL to the private repo containing your work, and (b) that you list every member in your group that worked on the assignment. 
 1. Finally, recall that **everyone** in the group submits an assignment on Blackboard individually, even though every member will supply me with the same requisite information (as usual).
 
When forking this repo, don't forget to choose the Advanced settings and place a check mark in the check box labeled "Inherit repository user/group permissions" to ensure I have proper access to your fork.
 
## Part 1: The Towers of Hanoi
 
The Towers of Hanoi is a classic problem in computer science. It illustrates dramatically the fact that for some kinds of problems the solution using a recursive algorithm is very easy to find, but a nonrecursive one is considerably more difficult.
 
The problem involves disks on three pegs, and to solve it you must move the disks from one peg to another according to the following rules:
 
 1. When moving a disk, you must put it on a peg -- you may not simply set it to the side to use later.
 1. You may move only one disk at a time, and it must be the top disk on one of the pegs.
 1. You may never put a larger disk on top of a smaller one.
 
It starts out as follows:

```
             A                          B                          C
             |                          |                          |
            <0>                         |                          |
           <-1->                        |                          |
          <--2-->                       |                          |
===============================================================================
```
 
Let's see this game in action!
Try this... (It may or may not work; it worked on both my Mac and my Cygwin installation) For a visualization of this game, start the `emacs` editor from either a Cygwin terminal window or from a Mac terminal window if you're using a Macintosh computer by simply typing `emacs` at the command prompt. Once the editor has loaded, try typing the command
 
```
ESC n ESC x hanoi
```
 
where ESC is the "Escape" key, `n` is the number of disks you wish to see in the visualization, and `x` and `hanoi` are literally typed. For example, if I chose 3 for `n` the final result would look like:
 
```
             |                          |                          |
             |                         <0>                         |
             |                        <-1->                        |
             |                       <--2-->                       |
===============================================================================
```
 
Hopefully that works for you; it's a neat visualization of the algorithm in action! Any way, back to the lab...
 
### The Base Case
 
The simplest case is when there is only one disk (located on peg A). You move the single disk from peg A to peg C and the problem is solved.
 
### The Inductive Case
 
For more than one disk, we have to use the inductive case, which should look something like this:

 1. Move the topmost `n - 1` disks from peg A to peg B, using peg C for temporary storage
 1. Move the final disk remaining on peg A to peg C (the base case)
 1. Move the `n - 1` disks from peg B to peg C, using peg A for temporary storage
 
Using the program built in this lab, answer the questions related to the Towers of Hanoi in the Questions.txt text file found in this repo. When you have answered all the questions related to the Towers of Hanoi, save your Questions.txt file and commit your changes.

## Part 2: Exploring different implementations for finding the nth Fibonacci Number

Using the program built in this lab, answer the questions related to the Fibonacci Numbers in the Questions.txt file found in this repo.
 