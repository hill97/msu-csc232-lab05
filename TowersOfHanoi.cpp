//
// Created by James Daehn on 9/26/16.
//

#include <iostream>
#include <iomanip>
#include "TowersOfHanoi.h"

TowersOfHanoi::TowersOfHanoi() : numMoves(0) {

}

void TowersOfHanoi::solve(int numDisks) {
//    const char PEG1 { 'A' };           // the three pegs
//    const char PEG2 { 'B' };
//    const char PEG3 { 'C' };
    move(numDisks, numMoves, PEG1, PEG3, PEG2);
}

int TowersOfHanoi::getNumMoves(int numDisks) {
    numMoves = 0; // reset
//    const char PEG1 { 'A' };           // the three pegs
//    const char PEG2 { 'B' };
//    const char PEG3 { 'C' };
    move(numDisks, numMoves, PEG1, PEG3, PEG2, true);
    return numMoves;
}

void TowersOfHanoi::move(unsigned n, unsigned &moveNumber, char source, char destination, char spare, bool silent) {
    if (n == 1) { // base case
        moveNumber++;
        if (!silent) {
            std::cout << std::setw(9) << moveNumber
                      << ". Move the top disk from " << source
                      << " to " << destination << std::endl;
        }
    } else { // inductive case
        move(n-1, moveNumber, source, spare, destination, silent);
        move(1, moveNumber, source, destination, spare, silent);
        move(n-1, moveNumber, spare, destination, source, silent);
    }
}