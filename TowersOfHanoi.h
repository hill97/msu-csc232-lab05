//
// Created by James Daehn on 9/26/16.
//

#ifndef MSU_CSC232_LAB05_TOWERSOFHANOI_H
#define MSU_CSC232_LAB05_TOWERSOFHANOI_H



class TowersOfHanoi {
public:
    TowersOfHanoi();
    void solve(int numDisks);
    int getNumMoves(int numDisks);
private:
    static const char PEG1 { 'A' };           // the three pegs
    static const char PEG2 { 'B' };
    static const char PEG3 { 'C' };
    unsigned numMoves;
    void move(unsigned n, unsigned & moveNumber, char source, char destination, char spare, bool silent = false);
};


#endif //MSU_CSC232_LAB05_TOWERSOFHANOI_H
